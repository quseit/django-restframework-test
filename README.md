# Coding challenge - API

We'd like to extend this Django REST Framework project (based on the one found
[here](https://github.com/beheshtraya/django_rest_sample)).

The API stores information about authors, categories and books.

We'd like to let our API users view library statistics, namely:

  1. Total number of books owned.
  2. Total cost of all books.
  3. Author with most books.
  4. Category with most books.

There are a few ways to approach this; feel free to choose one that seems
appropriate, and please explain why you think it's best for us.

We've also heard of at least one bug in the codebase. Can you try to identify
issues in the existing code, and document how you discovered and fixed the
bugs?

Also:

 - Please treat all code as production level code.
 - Please don’t spend more than 2 days on this. If time runs short, an
   explanation of what you would do given more time is fine.
 - Write an Dockerfile to deploy it with Docker into production env. (Optional)
 - Please send us the pull request link of your changes for the submission. (Important)

## Original `readme.txt`

    > pip install -r requirements.txt
    > python manage.py makemigrations
    > python manage.py migrate
    > python manage.py runserver
      -- server will be run on 127.0.0.1:8000
