from django.db import models


class Book(models.Model):
    title = models.CharField(max_length=255, null=True, blank=True)
    category = models.ForeignKey('Category', on_delete=models.CASCADE)
    author = models.ForeignKey('Author', on_delete=models.CASCADE) 
    owned = models.IntegerField()
    price = models.FloatField()
    publish_date = models.DateField()

    def __str__(self):
        return self.title

class Author(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.name

class Category(models.Model): 
    title = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.title

